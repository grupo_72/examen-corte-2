// Codificación del evento change

/*const monedaOrigen = document.querySelector('#monedaOrigen');

monedaOrigen.addEventListener('change', ()=>{
    const resultado = document.querySelector('#monedaDestino');
    if(resultado == 1){
    
    }
})*/


// Codificación del botón calcular
function calcular(){
    cant = document.getElementById('cantidad').value;
    monOrig = document.getElementById('monedaOrigen').value;
    monDest = document.getElementById('monedaDestino').value;
    subtot = document.getElementById('subtotal');
    totCom = document.getElementById('totalComision')
    totpag = document.getElementById('totalPagar');

    // de pesos a:
    if(monOrig == 1){

        if(monDest == 2){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 0.050).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 0.050;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
        else if(monDest == 3){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 0.068).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 0.068;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
        else if(monDest == 4){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 0.050).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 0.050;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
    }
    // de dolares americanos a:
    else if (monOrig == 2){

        if(monDest == 1){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 19.91).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 19.91;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
        else if(monDest == 3){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 1.36).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 1.36;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
        else if(monDest == 4){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 0.099).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 0.099;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
    }
    // de dolares canadienses a:
    else if (monOrig == 3){

        if(monDest == 1){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 14.69).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 14.69;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
        else if(monDest == 2){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 0.74).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 0.74;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
        else if(monDest == 4){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 0.73).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 0.73;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
    }
    // de euros a:
    else if (monOrig == 4){

        if(monDest == 1){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 20.08).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 20.08;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
        else if(monDest == 2){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 1.01).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 1.01;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
        else if(monDest == 3){
            let sub =0, tot =0;
            subtot.innerHTML = (cant * 1.37).toFixed(2);
            totalComision.innerHTML = (subtot.innerHTML * .03).toFixed(2);
            sub = cant * 1.37;
            tot = subtot.innerHTML * .03;
            totpag.innerHTML = (sub + tot).toFixed(2);
        }
    }
}

// Codificación del botón Registrar
function registro(){
    cant = document.getElementById('cantidad');
    monOrig = document.getElementById('monedaOrigen').value;
    monDest = document.getElementById('monedaDestino').value;
    subtot = document.getElementById('subtotal');
    totCom = document.getElementById('totalComision')
    totpag = document.getElementById('totalPagar');
    subTotGral = document.getElementById('subTotalGeneral');
    totComGral = document.getElementById('totalComisionGeneral');
    totPagGral = document.getElementById('totalPagarGeneral');

    subTotGral.innerHTML = "Subtotal: $" + subtot.innerHTML;
    totComGral.innerHTML = "Total Comisión: $" +totCom.innerHTML;
    totPagGral.innerHTML = "Total a Pagar: $" +totpag.innerHTML;

}

// Codificación del botón Borrar
function borrarRegistro (){
    cant = document.getElementById('cantidad');
    monOrig = document.getElementById('monedaOrigen').value;
    monDest = document.getElementById('monedaDestino').value;
    subtot = document.getElementById('subtotal');
    totCom = document.getElementById('totalComision')
    totpag = document.getElementById('totalPagar');
    subTotGral = document.getElementById('subTotalGeneral');
    totComGral = document.getElementById('totalComisionGeneral');
    totPagGral = document.getElementById('totalPagarGeneral');

    cant.value = "";
    monOrig.innerHTML = "";
    monDest.innerHTML = "";
    subtot.innerHTML = "";
    totCom.innerHTML = "";
    totpag.innerHTML = "";
    subTotGral.innerHTML = "";
    totComGral.innerHTML = "";
    totPagGral.innerHTML = "";
}

function validar (){
    cant = document.getElementById('cantidad').value;
    
    if(cant == 0){
        cant = document.getElementById('cantidad');
        alert("Por favor, Ingrese un valor válido");
        cant.value = "";
    }
}